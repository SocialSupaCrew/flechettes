package com.socialsupacrew.flechettes

import com.socialsupacrew.flechettes.ui.NewsListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { NewsListViewModel() }
}
