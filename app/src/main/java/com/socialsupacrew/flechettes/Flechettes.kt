package com.socialsupacrew.flechettes

import android.app.Application
import android.view.View
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class Flechettes : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@Flechettes)
            androidLogger()
            modules(appModule)
        }
    }
}
