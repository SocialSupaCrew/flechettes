package com.socialsupacrew.flechettes.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.socialsupacrew.flechettes.R
import com.socialsupacrew.flechettes.core.BaseActivity
import kotlinx.android.synthetic.main.activity_news_list.loadingContainer
import kotlinx.android.synthetic.main.activity_news_list.newsList
import org.koin.android.viewmodel.ext.android.viewModel

class NewsListActivity : BaseActivity() {

    private val viewModel: NewsListViewModel by viewModel()
    private val adapter by lazy { NewsListAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_list)

        viewModel.getStateLiveData().observe(this, Observer { applyState(it) })

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = RecyclerView.VERTICAL
        newsList.layoutManager = layoutManager
        newsList.adapter = adapter
    }

    private fun applyState(state: State) {
        Log.d("QSD", "state: $state")
        updateLoadingState(state.isLoading)

        if (state.newsEntities.isNotEmpty()) {
            adapter.submitList(state.newsEntities)
        }
    }

    private fun updateLoadingState(isLoading: Boolean) {
        loadingContainer.visibility = if (isLoading) View.VISIBLE else View.GONE
        newsList.visibility = if (isLoading) View.GONE else View.VISIBLE
    }
}
