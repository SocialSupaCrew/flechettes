package com.socialsupacrew.flechettes.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.socialsupacrew.flechettes.R
import com.socialsupacrew.flechettes.ui.NewsListViewModel.NewsEntities
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.news_item.image
import kotlinx.android.synthetic.main.news_item.subtitle
import kotlinx.android.synthetic.main.news_item.title

sealed class NewsListHolder(
    override val containerView: View
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    abstract fun bind(entities: NewsEntities?)
}

class NewsHeaderHolder(
    override val containerView: View
) : NewsListHolder(containerView) {

    constructor(parent: ViewGroup) : this(
        LayoutInflater.from(parent.context).inflate(R.layout.news_header, parent, false)
    )

    override fun bind(entities: NewsEntities?) {}
}

class NewsHolder(
    override val containerView: View
) : NewsListHolder(containerView) {

    constructor(parent: ViewGroup) : this(
        LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
    )

    override fun bind(entities: NewsEntities?) {
        entities as NewsEntities.News

        Glide.with(containerView)
            .load(entities.imageUrl)
            .placeholder(R.drawable.placeholder)
            .into(image)

        title.text = entities.title
        subtitle.text = entities.subtitle
    }
}

class NewsFooterHolder(
    override val containerView: View
) : NewsListHolder(containerView) {

    constructor(parent: ViewGroup) : this(
        LayoutInflater.from(parent.context).inflate(R.layout.news_footer, parent, false)
    )

    override fun bind(entities: NewsEntities?) {}
}


