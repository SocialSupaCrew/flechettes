package com.socialsupacrew.flechettes.ui

import android.view.ViewGroup
import com.socialsupacrew.flechettes.core.BaseAdapter
import com.socialsupacrew.flechettes.ui.NewsListViewModel.NewsEntities


class NewsListAdapter() : BaseAdapter<NewsListHolder>() {

    private var newsEntities = emptyList<NewsEntities>()
        set(value) {
            applyDiffUtil(field, value) { oldNews, newNews ->
                oldNews.id == newNews.id
            }
            field = value
        }

    override fun getDataCount(): Int = newsEntities.size

    override fun getDataViewType(position: Int): Int {
        return when (newsEntities[position]) {
            is NewsEntities.Header -> ViewType.HEADER.id
            is NewsEntities.News -> ViewType.NEWS.id
            is NewsEntities.Footer -> ViewType.FOOTER.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsListHolder {
        return when (viewType) {
            ViewType.HEADER.id -> NewsHeaderHolder(parent)
            ViewType.NEWS.id -> NewsHolder(parent)
            ViewType.FOOTER.id -> NewsFooterHolder(parent)
            else -> throw NoSuchElementException("$viewType doesn't match with any type")
        }
    }

    override fun onBindViewHolder(holder: NewsListHolder, position: Int) {
        if (newsEntities.isEmpty()) return
        if (holder is NewsHeaderHolder || holder is NewsFooterHolder) {
            holder.bind(null)
            return
        }
        holder.bind(newsEntities[position - headerSize()])
    }

    internal fun submitList(newsEntities: List<NewsEntities>) {
        this.newsEntities = newsEntities
    }

    enum class ViewType {
        HEADER,
        NEWS,
        FOOTER;

        val id: Int = ordinal
    }
}
