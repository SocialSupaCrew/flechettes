package com.socialsupacrew.flechettes.ui

import com.socialsupacrew.flechettes.core.BaseViewModel
import com.socialsupacrew.flechettes.ui.NewsListViewModel.NewsEntities.Footer
import com.socialsupacrew.flechettes.ui.NewsListViewModel.NewsEntities.Header
import com.socialsupacrew.flechettes.ui.NewsListViewModel.NewsEntities.News

class NewsListViewModel : BaseViewModel<State>() {
    override val defaultState: State =
        State()

    init {
        createList()
    }

    private fun createList() {
        reduce { it.copy(isLoading = true) }
        val newsList: MutableList<NewsEntities> = mutableListOf()
        newsList.add(Header)
        NewsStub.values().forEach {
            newsList.add(News(it.id, it.title, it.subtitle, it.image))
        }
        NewsStub.values().forEach {
            newsList.add(News(it.id, it.title, it.subtitle, it.image))
        }
        newsList.add(Footer)
        reduce { it.copy(isLoading = false, newsEntities = newsList) }
//        AndroidSchedulers.mainThread().scheduleDirect(
//            { reduce { it.copy(isLoading = false, newsEntities = newsList) } },
//            2,
//            TimeUnit.SECONDS
//        )
    }

    enum class NewsStub(val title: String, val subtitle: String, val image: String) {
        ONE(
            "Resulats championnat regionnal",
            "Certain sont restés trop longtemps à la buvette",
            "https://www.fourchette-et-bikini.fr/sites/default/files/168718452.jpg"
        ),
        TWO(
            "Assemblée Générale le 22/06/2020",
            "Y'aura des bières !",
            "https://www.brasserie-des-pics.fr/wp-content/uploads/2019/02/flechette-1024x683.jpg"
        ),
        THREE(
            "Entrainement de samedi prochain décalé",
            "À partir de 19h",
            "https://www.slate.fr/sites/default/files/styles/1060x523/public/flechettes.jpg"
        );

        val id = ordinal
    }

    sealed class NewsEntities(open val id: Int) {
        data class News(
            override val id: Int,
            val title: String,
            val subtitle: String,
            val imageUrl: String
        ) : NewsEntities(id)

        object Header : NewsEntities(Int.MIN_VALUE)
        object Footer : NewsEntities(Int.MAX_VALUE)
    }
}

data class State(
    val isLoading: Boolean = false,
    val newsEntities: List<NewsListViewModel.NewsEntities> = emptyList()
)
