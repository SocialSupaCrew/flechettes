package com.socialsupacrew.flechettes.core

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<VH : RecyclerView.ViewHolder>() : RecyclerView.Adapter<VH>() {

    private val headerViewTypeList: MutableList<Int> = mutableListOf()
    private val footerViewTypeList: MutableList<Int> = mutableListOf()

    final override fun getItemCount(): Int =
        headerViewTypeList.size + getDataCount() + footerViewTypeList.size

    override fun getItemId(position: Int) = position.toLong()

    final override fun getItemViewType(position: Int): Int {
        if (position < headerViewTypeList.size) {
            return headerViewTypeList[position]
        }

        var translatedPosition = position - headerViewTypeList.size
        val dataSize = getDataCount()
        if (translatedPosition < dataSize) {
            return getDataViewType(translatedPosition)
        }

        translatedPosition -= dataSize
        return footerViewTypeList[translatedPosition]
    }

    fun <T> applyDiffUtil(oldData: List<T>, newData: List<T>, compare: (T, T) -> Boolean) {
        val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return compare(oldData[oldItemPosition], newData[newItemPosition])
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return oldData[oldItemPosition] == newData[newItemPosition]
            }

            override fun getOldListSize() = oldData.size

            override fun getNewListSize() = newData.size
        })
        diff.dispatchUpdatesTo(this)
    }

    fun addHeader(viewType: Int, position: Int = headerViewTypeList.size) {
        headerViewTypeList.add(position, viewType)
        notifyItemInserted(position)
    }

    fun removeHeader(viewType: Int) {
        val position = headerViewTypeList.indexOf(viewType)
        if (position == -1) return
        headerViewTypeList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun addFooter(viewType: Int, position: Int = footerViewTypeList.size) {
        footerViewTypeList.add(position, viewType)
        notifyItemInserted(headerViewTypeList.size + getDataCount() + position)
    }

    fun removeFooter(viewType: Int) {
        val position = footerViewTypeList.indexOf(viewType)
        if (position == -1) return
        footerViewTypeList.removeAt(position)
        notifyItemRemoved(headerViewTypeList.size + getDataCount() + position)
    }

    fun containFooter(viewType: Int): Boolean {
        return footerViewTypeList.indexOf(viewType) != -1
    }

    fun isHeaderOrFooter(position: Int) = isHeader(position) || isFooter(position)

    fun isHeader(position: Int) = position < headerSize()
    fun isFooter(position: Int) = position >= headerSize() + getDataCount()

    fun hasHeader(): Boolean = headerViewTypeList.isNotEmpty()
    fun hasFooter(): Boolean = footerViewTypeList.isNotEmpty()

    fun headerSize(): Int = headerViewTypeList.size
    fun footerSize(): Int = footerViewTypeList.size

    abstract fun getDataCount(): Int
    abstract fun getDataViewType(position: Int): Int
}
