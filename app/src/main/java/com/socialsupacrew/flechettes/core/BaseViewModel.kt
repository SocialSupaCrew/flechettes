package com.socialsupacrew.flechettes.core

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

abstract class BaseViewModel<S> : ViewModel() {

    abstract val defaultState: S

    private val stateLiveData by lazy { MediatorLiveData<S>() }

    private val compositeDisposable = CompositeDisposable()

    fun getStateLiveData(): LiveData<S> = stateLiveData

    protected fun <T> addSource(liveData: LiveData<T>, onChanged: (S, T) -> S) {
        stateLiveData.addSource(liveData) { data ->
            reduce { onChanged.invoke(stateLiveData.value ?: defaultState, data) }
        }
    }

    protected fun reduce(reducer: (state: S) -> S) {
        stateLiveData.value = reducer.invoke(stateLiveData.value ?: defaultState)
    }

    protected fun reduce(
        reducer: (state: S) -> S, reducerToReset: (state: S) -> S, delay: Long = 200,
        timeUnit: TimeUnit = TimeUnit.MILLISECONDS
    ): Disposable {
        return Observable.timer(delay, timeUnit)
            .observeOn(AndroidSchedulers.mainThread())
            .map { reduce(reducerToReset) }
            .startWith(reduce(reducer))
            .subscribe({}, { Log.e(TAG, "error: ", it) })
    }

    protected fun toCompositeDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposableDispose()
    }

    protected fun compositeDisposableDispose() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
    }

    companion object {
        private const val TAG = "BaseViewModel"
    }
}
